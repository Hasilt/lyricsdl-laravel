<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tagmap extends Model
{
    protected $fillable=['song_id','tag_id'];

    public function song(){
        return $this->belongsTo('App\Models\Song');
    }
    public function tag(){
        return $this->belongsTo('App\Models\Tag');
    }
}
