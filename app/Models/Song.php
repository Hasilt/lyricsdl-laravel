<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    //

    protected $fillable=['name','artist','image','audio','lyrics','user_id','movie_id'];

    public function user(){
       return $this->belongsTo('App\User');
    }

    public function movie(){
       return $this->belongsTo('App\Models\Movie');
    }

    public function comments(){
       return $this->hasMany('App\Models\Comment');
    }

    public function tagmap(){
        return $this->hasMany('App\Models\Tagmap');
    }
    
}
