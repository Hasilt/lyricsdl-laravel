<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    //
    protected $fillable=['name','director','genre','user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function songs(){
        return $this->hasMany('App\Models\Song');
    }
}
