<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=['content','user_id','song_id'];

    public function user(){
        return $this->belongsTo('App\User');

    }
    
    public function song(){
        return $this->belongsTo('App\Models\Song');
    }

    
}
