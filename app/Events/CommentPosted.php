<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Http\Request;

class CommentPosted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(Request $request)
    {
        // dd($request);
        $this->request= $request;
    }

    
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
