<?php

namespace App\Repositories;

use App\Models\Song;

class SearchRepository
{
    public function searchSong($query)
    {
        $search_key = "%".$query."%";
        
        $result = Song::where('name','LIKE',$search_key)
                            ->orderBy('name','desc')
                            ->with('movie','user')
                            ->paginate(6);
    


        return Song::where('name','LIKE', $search_key)
                                ->orderBy('name','asc')
                                ->with('movie','user')
                                ->paginate(6);
                
    }
}