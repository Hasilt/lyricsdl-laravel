<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use DB;
class MoviesController extends Controller
{
    public function __construct(){
        
       $movies_list=array(DB::table('movies')->get());
       
    }
    public function insert(Request $request){
       
        $movie = New Movie;
        $movie->user_id = $request->user()->id;
        $movie->name = $request->movie_name;
        $movie->director = $request->director_name;
        $movie->genre = $request->movie_genre;
        $movie->save();
        return redirect()->back();
    }
}
