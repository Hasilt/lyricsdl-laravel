<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Song;
use App\Models\Comment;
use App\Events\CommentPosted;

class CommentsController extends Controller
{

   
    public function store(Request $request)
    {
        // dd($request);
        $comment = New Comment;
        $comment->user_id = $request->user;
        $comment->content = $request->comment;
        $comment->song_id = $request->song_id;
        $comment->save();

        

        event(new CommentPosted($request));
        
        return redirect()->back();


    }
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect()->back();
    }
}
