<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Auth;
use App\Models\Song;
use App\Models\Movie;
use App\Models\Tag;
use App\Models\Tagmap;
use App\User;
use DB;
use App\Repositories\SongsRepository;

class SongsController extends Controller
{
    protected  $songs;

    public function __construct(SongsRepository $songs)
    {
        $this->songs = $songs;
    }
    
    public function index(Request $request)
    {
    $songs = $request->user()->songs()->with('movie')->get();
    return view('lyrics_view',[
        'lyrics_list'=>$songs,
    ]);
    }

    public function create(Request $request)
    {
        
    }

    public function store(Request $request)
    {

        $movie = Movie::find($request->movie_id);
    
        $song = $movie->songs()->create([
            'user_id' => $request->user()->id,
            'name'=> $request->song_name,
            'audio' => '',
            'image'=>'',
            'artist'=>$request->artist_name,
            'lyrics'=>$request->lyrics

        ]);

        if($request->hasFile('img')&&$request->file('img')->isValid()) {
            $path = $request->img->store('images', 'local');
            $song->image = $path;
        }

        if($request->hasFile('audio')&&$request->file('audio')->isValid()) {
            $audio_path = $request->audio->store('audio', 'local');
            $song->audio = $audio_path;
        }

        $song->save();

        foreach($request->tags as $tag_name)
        {
            $tag = New Tag;
            $tag->name = $tag_name;
            $tag->save();
          
            $tagmap = New Tagmap;
            $tagmap->tag_id = $tag->id;
            $tagmap->song_id = $song->id;
            $tagmap->save();
            
        }
        return redirect('/home');
    }

    public function show(Request $request, Song $song)
    {
        
        $song_details = Song::where('id',$song->id)->with('movie','comments.user')->first();
       
        $movie_id = $song->movie->id;
        
        $related = Song::where('movie_id',$movie_id)->get();
        return view('lyrics_details',compact('song_details','related'));
    }

   
    public function edit($id)
    {
        $song = Song::find($id);
        $movie_list=DB::table('movies')->get();
        
        if(Auth::id()==$song->user_id)
        {
        return view('edit_lyrics',compact('song','movie_list'));
    
    }
    }

    public function update(Request $request,Song $song)
    {
        
        $song->name = $request->name;
        $song->movie_id = $request->movie_id;
        $song->artist = $request->artist;
        $song->lyrics = $request->lyrics;
        if($request->hasFile('image')&&$request->file('image')->isValid()) {
            if(file_exists(public_path()."/".$song->image)&& (($song->image)!=""))
            {
                unlink(public_path()."/".$song->image);
            }
            $path = $request->image->store('images', 'local');
            $song->image = $path;
        }
        if($request->hasFile('audio')&&$request->file('audio')->isValid()) {
            if(file_exists(public_path()."/".$song->audio)&& (($song->audio)!=""))
            {
                unlink(public_path()."/".$song->audio);
            }
            $audio_path = $request->audio->store('audio', 'local');
            $song->audio = $audio_path;
        }
        $song->save();
        return redirect('/home');
    }

    public function destroy(Song $song)
    {   
        $song->tagmap()->where("song_id",$song->id)->with('tag')->delete();
        $song->delete();

        return redirect('/home');
    }
}
