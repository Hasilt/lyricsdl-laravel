<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

class UserController extends Controller
{
   
    public function changePassword(Request $request)
    {
       // dd($request);
        $this->validate($request, [
            'oldpass' => 'required',
            'newpass' => 'required|min:6|confirmed'
        ]);
        
        $user = User::find(Auth::guard('users')->id());
        $current_password = $user->password;
        if(Hash::check($request->oldpass,$current_password))
        {
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            return redirect()->back();
        }else
        {
        $request->session()->flash('update_password', 'Task was not successful!');
        return redirect()->back();
        }
       
        
    }
}
