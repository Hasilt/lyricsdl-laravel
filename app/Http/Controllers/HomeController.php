<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Movie;
use Auth;

class HomeController extends Controller
{
    


    public function index()
    {     
          
           $movies_list=DB::table('movies')->get();
           $songs = Auth::user()->songs()->with('movie')->get();

           return view('home',[
               'movie_list'=>$movies_list,
               'lyrics_list'=>$songs,
               ]);
     }
  
}
