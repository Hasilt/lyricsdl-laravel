<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Repositories\SearchRepository;
use App\Models\Song;
class SearchController extends Controller
{
    protected $search;

    public function __construct(SearchRepository $search)
    {
        $this->search = $search;
    }

    public function search(Request $request)
    {
       
        $key = $request->value;
        $search_result = $this->search->searchSong($request->value);
    
        return view('results',compact('key','search_result'));

    }
    public function searchSugg(Request $request)
    {
    //   dd($request);
        $key = "%".$request->value."%";
        $search_sugg = Song::where('name','LIKE',$key)->get();
         foreach($search_sugg as $sugg)
        {
            return $sugg->name;
        

         }
        // return view('seach_sugg')->with('search_sugg',$search_sugg);
        
    //    return response()->json($returnHTML);
    }
}
