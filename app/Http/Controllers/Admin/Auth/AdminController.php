<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Session\SessionManager;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
     use AuthenticatesUsers;

    
     
    public function logout(Request $request){
      
        
        Auth::guard('admin')->logout();
        
        return redirect()->route('front::welcome_page');
       
    }
   
}
