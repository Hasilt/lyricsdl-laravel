<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
class AdminLoginController extends Controller
{
     use AuthenticatesUsers;
  
    public function showLoginForm()
    {
      return view('admin.login');
    }
    public function login(Request $request)
    {

        $this->validate($request, [
          'username'   => 'required|email',
          'password' => 'required|min:6'
        ]);
      
      if (Auth::guard('admin')
            ->attempt(['username' => $request->username, 'password' => $request->password])) {
        return redirect()->intended(route('admin::admin.dashboard'));
      }
    
      return redirect()->back()->withInput($request->only('username','password'));
    }

   
}
