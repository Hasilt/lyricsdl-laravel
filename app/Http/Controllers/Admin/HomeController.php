<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Comment;
use App\Models\Song;
use App\Models\Movie;
use App\User;



class HomeController extends Controller
{
    public function __construct(){
        // if(Auth::)
    }
  
    public function dashboard(){

        $total_comments = Comment::count();
        $total_Songs = Song::count();
        $total_movies = Movie::count();
        $total_users = User::count();
        
        return view('admin.dashboard',[
             'total_comments' => $total_comments,
             'total_songs' => $total_Songs,
             'total_movies' => $total_movies,
             'total_users' => $total_users,
         ]);
     }
      
}