<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Song;
use App\Models\Movie;
use App\User;
use App\Repositories\SongsRepository;

class SongsController extends Controller
{
   
    public function index()
    {
       $songs = Song::with('movie')->get();
       return view('admin.songs',[
           'lyrics_list'=>$songs,
       ]);
    }
  
    public function destroy(Song $song)
    {
        
        $song->delete();

        return redirect()->back();
    }
}
