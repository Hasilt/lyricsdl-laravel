<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Movie;
use App\User;
use App\Repositories\SongsRepository;

class MoviesController extends Controller
{
   
    public function index()
    {
       $movies = Movie::with('user')->get();
    //    dd($movies);
       return view('admin.movies',[
           'movie_list'=>$movies,
       ]);
    }
}
