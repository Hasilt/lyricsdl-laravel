<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Auth;
use App\Models\Comment;

class CommentsController extends Controller
{

    public function index()
    {
    $comments = Comment::with('user','song')->get();
    return view('admin.comment',[
        'comment_list'=>$comments,
    ]);
    }
    
    public function destroy(Comment $comment)
    {

        $comment->delete();

        return redirect()->back();
    }
}
