<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard 
     * @return mixed
     */
     public function handle($request, Closure $next, $guard = null)
     {
   
         if (Auth::guard($guard)->check()) 
         {
            if(! $guard == 'admin')
                    {
                        return redirect()->route('admin::admin.login');
                    }
         }else return redirect()->route('admin::admin.login');
         
         return $next($request);
     
}
}