<?php

namespace App\Listeners;

use App\Events\CommentPosted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AfterCommentPosted
{
   
    public function __construct()
    {
        //
    }

 
    public function handle(CommentPosted $event)
    {
       
        $message = "a new comment for song with id ".$event->request->song_id;
        Storage::append('comment-log.txt',$message);
    }
}
