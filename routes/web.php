<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  

Route::group(['prefix' => 'admin', 'as' => 'admin::', 'namespace'=>'Admin'],function(){

    Route::get('login',['as'=>'admin.login','uses'=>'Auth\AdminLoginController@showLoginForm']);
    Route::post('login', ['as'=>'admin.login.submit', 'uses'=>'Auth\AdminLoginController@login']);

    Route::group(['middleware' => 'admin:admin'],function(){

        Route::get('/',['as'=>'admin.dashboard', 'uses'=>'HomeController@dashboard']);

        Route::post('logout', ['as'=>'admin.logout', 'uses'=>'Auth\AdminController@logout']);

        Route::resource('songs','SongsController');

        Route::get('/movies',['as'=>'movies.index','uses'=>'MoviesController@index']);
        Route::resource('comments','CommentsController');
        Route::get('/users',['as'=>'users.index','uses'=>'UserController@index']);
        
    });

});




Route::group(['as' => 'front::'],function(){
Route::get('/',['as'=>'welcome_page', 'uses'=>'WelcomeController@welcome']);

//search
Route::get('search',['as'=>'search', 'uses'=>'SearchController@search']);
Route::post('/searchsugg',['as'=>'search.sugg','uses'=>'SearchController@searchSugg']);

//login
Route::get('login',['as'=>'login','uses'=>'Auth\LoginController@showLoginForm']);
Route::post('login', ['as'=>'login.submit','uses'=>'Auth\LoginController@login']);

//register
Route::get('register', ['as' => 'register','uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::post('register', ['as' => 'register','uses' => 'Auth\RegisterController@register']);

//passwordreset
Route::get('password/reset', ['as' => 'password.request','uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('password/reset', ['as' => '','uses' => 'Auth\ResetPasswordController@reset']);
Route::get('password/reset/{token}', ['as' => 'password.reset','uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::get('home',['as'=>'home', 'uses'=>'HomeController@index']);
Route::group(['middleware'=>'auth'],function() {
        Route::get('home',['as'=>'home', 'uses'=>'HomeController@index']);
        Route::post('insert',['as'=>'movie.insert', 'uses'=>'MoviesController@insert']);
        Route::post('/change_password',['as'=>'new_password', 'uses'=>'UserController@changePassword']);
        Route::post('/logout', ['as'=>'logout', 'uses'=>'Auth\LoginController@logout']);
        Route::resource('comments','CommentsController');
        Route::resource('songs','SongsController');
});

});