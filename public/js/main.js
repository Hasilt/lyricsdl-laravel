$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {
    $("#uploads").on("click", "#deletebutton", function() {
        // event.preventDefault();
        var value = $(this).attr("value");
        var token = $(this).data('token');
        console.log(token);
        console.log(value);
        var ans = confirm("Delete this post??");
        if (ans) {
            $.ajax({
                url: "front::songs/" + value,
                type: 'POST',
                data: {
                    _method: 'delete',
                    _token: 'token'
                },
                succcess: function(data) {
                    location.reload();
                    return false;
                }
            });
        }
    });


    var length = $('#tag-fields').length;

    $('#add-field-icon').unbind().click(function() {
        event.stopPropagation();
        console.log(length);
        if (length < 10) {
            length++;
            var TagsField = $("#tags-field");
            var field = $("<div id ='tag' class='tag-field'><input type = 'text' class = 'tag-form' placeholder = 'Add tag here' name = 'tags[]' id = 'input-field' ><a id ='delete-field-icon' class='delete-field-icon'><i class='fa fa-trash'></i></a></div>");
            field.attr('id', "tag-" + length);

            $(TagsField).append(field);

            $('.delete-field-icon').css('visibility', 'visible');

            $('.delete-field-icon').unbind().click(function(event) {
                event.preventDefault();
                event.stopPropagation();

                $(this).parent('div').remove();
                length--;
                console.log('check:length=' + length);
                if (length === 1) {
                    console.log("inside l=1 if");
                    $('.delete-field-icon').css('visibility', 'hidden');
                }
            });
        } else {
            alert("maximum 10 tags allowed");
        }
    });


});

$("#comment-box").on("click", function() {
    $("#comment-submit").css("visibility", "visible");
});

$("#movies_list").on("change", function() {
    // console.log('check');
    $("select option:selected").each(function() {
        var value = $(this).val();
        console.log(value);
        if (value === 'new') {
            $("#movie-add-dialog").dialog();

        }
    });
});

$("#sbox").on("keyup onchange", function() {
    var sKey = $("#sbox").val();
    var token = $("#csrf").attr('content');
    console.log(sKey);
    console.log(token);
    if (sKey.length > 0 && $(window).width() >= 479) {
        $("#search-sugg").show();
        jQuery.ajax({
            url: "front::searchsugg",
            data: {
                "value": sKey,

            },
            type: "POST",
            success: function(data) {
                console.log(data);
                $("#search-sugg").html(data);
            },
            error: function() {
                $("#search-sugg").html("nothing found");
            }
        });
    } else if (sKey.length == 0) {
        $("#search-sugg").hide();
    }
});