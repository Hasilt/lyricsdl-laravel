@extends('layouts.app')
@section('content')
 <div class="container">

        
        <div class="row">
            <div class="col-lg-12">
                 <img class="img img-responsive img-fluid" src="{{URL::asset($song_details->image)}}" alt="Image not found" id="lr-details-cover-img">
               <h1 class="page-header lyrics-heading"> {{$song_details->name}}
                    <small>{{$song_details->movie->name}}</small>
                  
                    @if(($song_details->audio)!="")
                   
                        <div id="audio-player">
                         <audio controls>
                    <source src="{{URL::asset($song_details->audio)}}" type="audio/mpeg">
                </audio></div>
                    @endif
                  
                    @if(Auth::check())
                    @if(Auth::id()==$song_details->user_id)
                    <form action="{{route('front::songs.edit',['songs'=>$song_details->id])}}" method="get" id="lr-details-edit-button">
                    <input type="hidden" value="{{$song_details->id}}" name="snid"></input>
                <button id="edit-button" type="submit" class="btn btn-info"><span class="glyphicon glyphicon-edit">Edit</span></button></form>
                  @endif
                  @endif
                  </div>
                </h1>
            </div> </div>
        <div class="row">

            <div class="col-md-9 col-sm-12 col-xs-12 lr-view">
                <h3 id="lr-details-header">Lyrics</h3>
                <div class="lyrics-view">
                <pre id="lview">{{$song_details->lyrics}}</pre>
                </div>
                </div>

            <div class="col-md-3">
                <div class="row">
                    <div class="col-sm-10 col-xs-6 col-md-12">
                        <h3>song_details info</h3>
                <ul>
                    <li>Singer:{{$song_details->artist}}</li>
                    <li>Director:{{$song_details->movie->director}}</li>
                    <li>Genre:{{$song_details->movie->genre}}</li>
                </ul>
                        </div>
                </div>

                  
        
               <h3>Related Items</h3>
                @foreach($related as $related_song_details)
                @if(($related_song_details->id) !=($song_details->id) && ($loop->index <= 4))
                                         
                <div class="row" id="related-items">
                <div class="col-sm-12 col-xs-12 col-md-12">
                <a href="{{$related_song_details->id}}">
              
                <div class="col-md-4 col-sm-8">
                    <img class="img-responsive related-songs-item" src="{{URL::asset($song_details->image)}}" alt=""></div>
                    <div class="col-sm-4 col-md-4">
                          <h4>{{$related_song_details->name}}</h4>
                        </div>
                </a>
            </div></div>
            @endif
            @endforeach
                 
            
              </div>
              
                  <div class="col-md-9 col-sm-12 col-xs-12 comments-section">
                      <div class="comment-input">
                           @if( Auth::check() )
                      <h4>Add a comment as {{Auth::user()->name}}:</h4>
                       @else  <h4> Add an Anonymous comment:
                       @endif
                      <form class="form-group" action="{{route('front::comments.store')}}" method="post" id="comment-form">
                         {{csrf_field()}}
                         
                           @if(Auth::check())
                          <input type="hidden" name="user" value="{{Auth::user()->id}}">
                          @endif
                          <input type="hidden" name="song_id" value="{{$song_details->id}}">
                          <textarea class="form-group form-control" name="comment" id="comment-box" placeholder="click here to add a comment"></textarea>
                          <button type="submit" class="btn btn-success" id="comment-submit">Submit</button>
                          </form><hr id="divider">
                          </div><hr>
                          
                        <div class="comments">
                       
                    
                      @if(!empty($song_details->comments))
                        @foreach ($song_details->comments as $comment)
                         
                            @if(($comment->user)==null)
                                @php $author = "Anonymous"; @endphp
                            @else @php $author = $comment->user->name; @endphp  
                             @endif
                              <div class="row user-comments">
                                  <div class="col-md-12 col-sm-12 col-xs-12 comment-box">        
                                       <h4> {{$author}}</h4>
                                       <div class="comment-content">
                                           {{$comment->content}}
                                           </div>
                                           <div class="comment-meta">
                                               Comment by  {{$author}}  on {{$comment->created_at}}; 
                                               </div>
                                       </div> 
                                        </div>                 
                       
                        @endforeach
                        @endif
                    </div>
                  </div>
                  </div>

            </div>

        </div>
      <hr>
@endsection('content')