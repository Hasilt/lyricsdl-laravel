@extends('layouts.app')
@section('content')
<div class="container" id="edit-container">
    <div class="row">
        	<div class="col-md-6 col-xs-12 col-sm-12">
                    <h2> Edit {{$song->name}}</h2>
					<form action="{{route('front::songs.update',['songs'=>$song->id])}}" method='post' enctype='multipart/form-data'>
		                 {{csrf_field()}}
                     {{method_field('PUT')}}
                        <div class='form-group animated'>
		                  <div class='container'>
		                    <label for='sn'><b> Song:</b></label>
		                    <input type='text' class='form-control form_upload' placeholder='Song name' name='name' id='sn' required value="{{$song->name}}">
		                   <div class="form-group form_upload">
                            <label for='mn'><b> Movie: </b></label>
		                     <select class="form-control form_upload" name="movie_id" id="movies_list">
                          @foreach($movie_list as $movie)
                          
                            <option value="{{ $movie->id }}">{{ $movie->name }}</option>
                          @endforeach
                            <option>Enter new movie</option>
                        </select>
                        </div>
												<label for='ssnm'><b>Artist </b></label>
												<input type='text' class= 'form-control form_upload' placeholder='Enter singer name' name='artist' id='ssnm' value="{{$song->artist}}">
												<label for='lr'> Lyrics:</label>
												<textarea class='form-control form_upload' rows='5' id='lyr' name='lyrics' placeholder='Enter lyrics here' id='lr' >{{$song->lyrics}}</textarea>
												<div class="button-inline">
								<label for='img1' class='control-label image-edit-label'> Select image </label>
							<input type='file' id='img1' name='image' placeholder='select image'>
							<label class="custom-file" for="audio"> Select Audio file</label>
                    <input type="file" id="audio" name="audio" class="custom-file-input" placeholder="select audio">
					<span class="custom-file-control"></span>
					</div>
							<button type='submit' class='btn btn-lg btn-primary' value='submit' id='up_lyrics'>update</button>
							
		                  </div> 
		                </div>
					  </form></div>

@endsection('content)