<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Lyrics Search</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <!-- Styles -->
    <link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Styles -->
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('front::login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ route('front::home') }}">Home</a>
                    @else
                        <a href="{{ route('front::login') }}">Login</a>
                        <a href="{{ route('front::register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                 <div class="col-md-6 col-md-offset-3">
                  <h3> Lyrics Search </h3>
                    <form action="{{route('front::search')}}" method="get" class="form">
            <div class="form-group">
              <div class="input-group">
              <input type="text" name="value" placeholder="enter query" class="form-control animated" id="sbox">
               
               <span class="input-group-btn">
              <input type="submit" value="search" class="btn btn-secondary">
                <span class="glyphicon glyphicon-search"></span>
               </button></span>
               </div>
               
             </div>
                
               </div>
            </div>            
          </form>
                </div>

               
            </div>
        </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/main.js')}}" type="text/javascript"></script>
    </body>
</html>
