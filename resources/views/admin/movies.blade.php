@extends('admin.header.admin')
@section('content')
<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Movies</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        User uploads
                    
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Movie Name</th>
                                    <th>Director</th>
                                    <th>Genre</th>
                                    <th>User</th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($movie_list as $movie)
                                <tr>
                                <td>{{$movie->name}}</td>
                                <td>{{$movie->director}}</td>
                                <td>{{$movie->genre}}</td>
                                <td>{{$movie->user->name}}</td>
                                
                                </tr>
                                @endforeach
                            </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
                            
@endsection