@extends('admin.header.admin')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Comment</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    User Comment
                
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Song</th>
                                <th>Comment</th>
                                <th>User</th>
                                <th>Action</th>
                            </tr>

                        </thead>
                        <tbody>
                            @foreach($comment_list as $comment)
                            <tr>
                            <td>{{$comment->id}}</td>
                            <td>{{$comment->song->name}}</td>
                            <td>{{$comment->content}}</td>
                            @if($comment->user == null)
                            <td>Anonymous</td>
                            @else
                            <td>{{$comment->user->name}}</td>
                            @endif
                            <form action="{{route('admin::comments.destroy',['comment'=>$comment->id])}}" method="post" id="delete">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            
                            <td class="center"><button type="submit" class="btn btn-danger" ><i class="fa fa-trash"></i></button></td>
                            </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
                        
@endsection