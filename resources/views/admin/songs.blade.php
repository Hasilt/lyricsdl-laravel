@extends('admin.header.admin')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Songs</h1>
        </div>
      
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    User uploads
                
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Song Name</th>
                                <th>Artist</th>
                                <th>Movie Name</th>
                                <th>Movie Director</th>
                                <th>Genre</th>
                                <th>Action</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($lyrics_list as $song)
                            <tr>
                            <td>{{$song->name}}</td>
                            <td>{{$song->artist}}</td>
                            <td>{{$song->movie->name}}</td>
                            <td>{{$song->movie->director}}</td>
                            <td>{{$song->movie->genre}}</td>
                            <form action="{{route('admin::songs.destroy',['songs'=>$song->id])}}" method="post" id="delete">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                            <td class="center"><button type="submit" class="btn btn-danger" ><i class="fa fa-trash"></i></button></td>
                                </form>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection