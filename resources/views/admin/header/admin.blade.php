<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta id="csrf" name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Laravel') }}</title>

<!-- Styles -->
<link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet">
<link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('css/admin/metisMenu.min.css')}}" rel= "stylesheet">
<link href="{{URL::asset('css/admin/admin-custom.css')}}" rel= "stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel= "stylesheet">
<!-- Styles -->
</head>
<body>  
        <div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">Admin</a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            @if (Auth::guard('admin')->guest())
                <li><a href="{{ route('admin::admin.login') }}">Login</a></li>
            @else
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-w"></i>  {{ Auth::guard('admin')->user()->name }}</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="{{ route('admin::admin.logout') }}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                <form id="logout-form" action="{{route('admin::admin.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                    </li>
            @endif
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    
                <li>
                <a href="{{route('admin::admin.dashboard')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                <a href="{{route('admin::songs.index')}}"><i class="fa fa-music fa-fw"></i> Songs</a>
                </li>
                <li>
                <a href="{{route('admin::movies.index')}}"><i class="fa fa-video-camera fa-fw"></i> Movies</a>
                </li>
                <li>
                <a href="{{route('admin::users.index')}}"><i class="fa fa-user fa-fw"></i> Users</a>
                </li>
                <li>
                <a href="{{route('admin::comments.index')}}"><i class="fa fa-edit fa-fw"></i> Comments</a>
                </li>
                </ul>
               
                </li>
                </ul>
            </div>
            
        </div>
        <!-- /.navbar-static-side -->
    </nav>
</div>
    @yield('content')


<!-- Scripts -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ asset('/js/admin/admin.js')}}" type="text/javascript"></script>

</body>

</html>