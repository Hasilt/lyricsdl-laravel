@extends('layouts.app')

@section('content')
<div class="container">
     
          <div id="tabview">
            <div class="row">
              
              <div class="col-md-9"> <h4>
            <ul class="nav nav-pills">
              <li class="active">
                <a href="#overview" class="active" data-toggle="tab">OverView </a>
              </li>
              <li>
                <a href="#upload" data-toggle="tab"> Add Lyrics </a>
              </li>
              <li>
                <a href="#settings" data-toggle="tab"> Settings </a>
              </li>
            </ul></h4></div>
          <div class="col-md-8">
          <div class="tab-content clearfix">
            <div class="tab-pane active" id="overview">
            
              <h2><br> Uploads </br></h2>
              <hr>
                               
             <div id="uploads">
             @include('lyrics_view')
              </div>
            </div>
            <div class="tab-pane" id="upload">
              <h2><br> Add new lyrics</br></h2>
              <hr>
              <form action="{{route('front::songs.store')}}" method="post" enctype="multipart/form-data">
                 {{csrf_field()}}
                <div class="form-group col-xs-12 col-md-10 animated blog-main">
                  <div class="container"> 
                    <label for="sn"><b> Song:</b></label>
                    <input type="text" class="form-control form_upload" placeholder="Song name "name="song_name" id="sn" required>
                    {{--  <label for="mn"><b> Movie: </b></label>
                    <input type="text" class="form-control form_upload" placeholder="Movie? "name="movie_id" id="mn"></br>   --}}
                    {{--  <select>
                    <option>1</option>
                    </select>  --}}
                      
                    <div class="form-group">
                    <label for="movies_list"><b> Movie:</b></label>
                        <select class="form-control" name="movie_id" id="movies_list">
                          @foreach($movie_list as $movie)
                          
                            <option value="{{ $movie->id }}">{{ $movie->name }}</option>
                          @endforeach
                            <option value="new">Enter new movie</option>
                        </select>
                      </div>

                    <label for="ssnm"><b> Artist: </b></label>
                    <input type="text" class= "form-control form_upload" placeholder="Enter singer name "name="artist_name" id="ssnm">
                   
                    <label for="lr"> Lyrics:
                    <textarea class="form-control form_upload" rows="9" id="lyr" name="lyrics" placeholder="Enter lyrics here" id="lr"></textarea>
                    <hr>
                   
                    <div class="row" id="tag-fields">
                    <div class="col-md-8">
                      <label for="tag"><b>Tags:</b></label>
                        <div id="tags-field">
                          <div id ='tag' class='tag-field'><input type = 'text' class = 'tag-form' placeholder = 'Add tag here' name = 'tags[]' id = 'input-field'>
                          <a id ='delete-field-icon' class='delete-field-icon' style='margin-left: 7px;'><i class='fa fa-trash'></i></a>
                          </div>

                          {{--  <div id ="delete-field-icon" class="delete-field-icon"><i class="fa fa-trash"></i></div> --}}
                        </div>  
                      </div>
                  
                    <div class="col-md-2">
                        <div id ="add-field-icon" class="add-field-icon"><i class="fa fa-plus"></i></div>
                    </div>
                    </div>
                    
                    
                    <label class="custom-file">
                    <input type="file" id="img1" name="img" class="custom-file-input"> Select Image</label>
                    <span class="custom-file-control"></span>
                    <label class="custom-file">
                    <input type="file" id="audio" name="audio" class="custom-file-input"> Select Audio File</label>
                    <span class="custom-file-control"></span>
                    <br><button type="submit" class="btn btn-primary btn-lg" value="submit" id="up-submit">Submit</button></br>
                  </div>
                </div>
              </form>
            </div>
            <div id="movie-add-dialog" title="Add new movie">
             <form action="{{route('front::movie.insert')}}" method="post">
              {{csrf_field()}}
              <label for="movie_name"><b>Name: </b></label>
              <input type ="text" name="movie_name" class="form-control" id="movie_name">
              <label><b>Movie director: </b></label>
              <input type ="text" name="director_name" class="form-control">
              <label><b> Genre: </b></label>
              <input type ="text" name="movie_genre" class="form-control">
              <br><button type="submit" class="btn btn-primary btn-lg" value="submit" id="up-submit">Submit</button></br>
             </form>
            </div>
              <div class="tab-pane" id="settings">
                <h2><br> User Settings </br></h2>
                <hr>
                <p><h4>Change password </h4>  
                <form action="{{url('change_password')}}" method="post" id="chgpwd">
                {{csrf_field()}}
                    <div class="container">
                <div class="form-group col-xs-12 col-md-10 animated blog-main form_upload">
                    <label for="passw1"><b> Enter current password:</b></label>
                    <input type="password" class="form-control passval" placeholder="New password" name="oldpass" id="opass" required>
                    @if ($errors->has('oldpass'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('oldpass') }}</strong>
                                    </span>
                                @endif
                   <label for="passw1"><b> Enter new password:</b></label>
                    <input type="password" class="form-control passval" placeholder="New password"name="newpass" id="npass1" required>
                   @if ($errors->has('newpass'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('newpass') }}</strong>
                                    </span>
                                    @endif
                                
                            
                   <label for="passw2"><b> Repeat password: </b></label>
                    <input type="password" class="form-control passval" placeholder="Repeat password "name="newpass_confirmation" id="npass2" required>
                     @if ($errors->has('newpass_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('newpass_confirmation') }}</strong>
                                    </span>
                                    @endif
                               
                           
                    {{--  <div class="help-block with-errors" id="message1"></div>  --}}

                    <button type="submit" class="btn btn-primary btn-lg" value="submit" id="updatepass">Update</button>
                  
                </div>
              </div>
            </form></p>
              </div>
            </div>
            
            </div> </div>
            
        
@endsection('content')
