@extends('layouts.app')
@section('content')
<div class='container'>
    <div class="row">
	<hgroup class='mb20'>
	<h2> search results </h2>
	<h3 class='lead'><strong>{{$search_result->total()}}</strong> results found for your query "{{$key}}"</h3>
	</hgroup>
    <hr></div>
    </div>
		<div class="container">
            <div class="row dropdown-filter-row">
                <div class="col-md-12">
            
             {{--  <div class="dropdown-filter">
                <button type="button" class="btn btn-secondary dropdown-toggle dropdown-menu-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="sort-button">
                 Order by
                </button>
                  <div class="dropdown-menu filter-menu dropdown-menu-right">
                    <ul class="list-unstyled">
                    <li><a class="dropdown-item filter-menu-item" href="">Ascending</a></li>
                    <li><a class="dropdown-item filter-menu-item" href="">Descending</a></li></ul>
                </div>  
            </div>   --}}
                   </div>
            </div>
           
		<div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12">
          
                
                
                <ul class="list-unstyled" id="search-result-list">
                @foreach($search_result as $result)
				<li>
                <div class='col-xs-12 col-sm-4 col-md-4 result-box'>
                    <div class="thubmnail box">
                     <h2><a href="{{route('front::songs.show',['songs'=>$result->id])}}" target="_blank" id="name-of-song"> {{$result->name}} </a></h2>
                        <a href="{{route('front::songs.show',['songs'=>$result->id])}}" target="_blank">
                        <img src="{{URL::asset($result->image)}}" class='img img-thumbnail result-image' alt='Image not available'></img></a>
                     
						<div class="row">
							<div class="col-md-6 col-xs-6 col-sm-4 movie-name">
								<h4>Movie: <label> {{$result->movie->name}}</label></h4>
                                <h4> Artist: {{$result->artist}} </h4>
                            </div>
							<div class="col-md-6 col-xs-6 col-sm-4">
								
                                <h4> Uploaded by:{{$result->user->name}}</h4>
                            </div>
                        </div>
                    </div>
                        
            </div></li>
            @if((($loop->index +1) % 3) == 0)
                <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
            @endif
              @endforeach
            </ul>
          
			
   <hr>
    <span class='clearfix border'></span>
    <div class="container">
	<div class="row">
        <div class="col-md-7 col-md-offset-4">
            {{ $search_result->links() }}
    </div>
    </div>
    </div>

@endsection('content')